﻿using AppConfiguration.Core;
using AppConfiguration.Program.Helpers;
using AppConfiguration.Sections.Endpoints;
using AppConfiguration.Sections.Logger;
using System;

namespace AppConfiguration.Program
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // Initialize DependencyInjections (specifically project`s config files loader)
            DependencyHelper.Initialize();

            // Get configuration property from singletone instance of section model
            Console.WriteLine($"Logger path: {LoggerSection.Instance.Path}");
            Console.WriteLine($"Logger max level: {LoggerSection.Instance.Levels.Max}");
            Console.WriteLine($"Notes url: {EndpointsSection.Instance.Urls[Url.Notes]}");
            
            // Save all loaded config to C:\Configs\config_{CurrentDate}.log
            // P.S. Place a breakpoint here to see whats was loaded to AppConfigurationManager
            AppConfigurationManager.Configuration.SaveToFile("C:\\Configs", $"config_{DateTime.Now:dd.MM.yyyy}.log");
            
            Console.ReadKey(true);
        }
    }
}
