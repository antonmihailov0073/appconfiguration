﻿using AppConfiguration.Common;
using AppConfiguration.Program.Infrastructure;

namespace AppConfiguration.Program.Helpers
{
    public class DependencyHelper
    {
        public static void Initialize()
        {
            DependencyProvider.InitializeUsing(new DependencyRegistrator());
        }
    }
}
