﻿using AppConfiguration.Core;
using AppConfiguration.Core.Base;

namespace AppConfiguration.Program.Infrastructure
{
    class AppConfigurationLoader : AConfigurationLoader
    {
        protected override void OnLoading(AppConfigurationBuilder builder)
        {
            builder.UseStandartLocalPath()
                .IncludeJsons(
                    ConfigurationFiles.Endpoints,
                    ConfigurationFiles.AppSettings
                )
                .UseStandartSharedPath()
                .IncludeJsons(
                    ConfigurationFiles.Logger
                );
        }
    }
}
