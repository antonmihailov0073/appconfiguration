﻿using AppConfiguration.Common.DependecyInjections;
using AppConfiguration.Core.Interfaces;
using SimpleInjector;

namespace AppConfiguration.Program.Infrastructure
{
    public class DependencyRegistrator : IDependencyRegistrator
    {
        public Container GetConfiguredContainer()
        {
            var container = new Container();

            container.Register<IConfigurationLoader, AppConfigurationLoader>();

            return container;
        }
    }
}
