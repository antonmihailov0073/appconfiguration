﻿using AppConfiguration.Common.DependecyInjections;
using SimpleInjector;

namespace AppConfiguration.Common
{
    public static class DependencyProvider
    {
        private static Container _container;


        public static bool IsInitialized => _container != null;


        public static TDependency Resolve<TDependency>()
            where TDependency : class
        {
            return _container.GetInstance<TDependency>();
        }

        public static void Register<TDependency, TInstance>()
            where TDependency : class
            where TInstance : class, TDependency
        {
            _container.Register<TDependency, TInstance>();
        }


        public static void InitializeUsing(IDependencyRegistrator registrator)
        {
            _container = registrator.GetConfiguredContainer();
        }
    }
}
