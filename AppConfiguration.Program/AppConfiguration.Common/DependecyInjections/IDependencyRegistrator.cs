﻿using SimpleInjector;

namespace AppConfiguration.Common.DependecyInjections
{
    public interface IDependencyRegistrator
    {
        Container GetConfiguredContainer();
    }
}
