﻿namespace AppConfiguration.Sections.Logger
{
    public enum LogLevel
    {
        Debug,
        Info,
        Error,
        Fatal
    }
}
