﻿using AppConfiguration.Core;
using AppConfiguration.Core.Base;
using Newtonsoft.Json;

namespace AppConfiguration.Sections.Logger
{
    [JsonObject(MemberSerialization.OptIn)]
    public class LoggerSection : ASection
    {
        private const string SectionName = "logger";


        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("levels")]
        public LevelsSettings Levels { get; set; }


        public static LoggerSection Instance { get; } = AppConfigurationManager
            .Configuration
            .GetSection<LoggerSection>(SectionName);
    }
}
