﻿using Newtonsoft.Json;

namespace AppConfiguration.Sections.Logger
{
    [JsonObject(MemberSerialization.OptIn)]
    public class LevelsSettings
    {
        [JsonProperty("min")]
        public LogLevel Min { get; set; }

        [JsonProperty("max")]
        public LogLevel Max { get; set; }
    }
}
