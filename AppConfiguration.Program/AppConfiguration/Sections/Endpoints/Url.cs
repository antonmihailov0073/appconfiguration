﻿namespace AppConfiguration.Sections.Endpoints
{
    public enum Url
    {
        Notes,
        Auth
    }
}
