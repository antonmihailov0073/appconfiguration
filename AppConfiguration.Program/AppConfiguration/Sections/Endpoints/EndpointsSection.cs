﻿using AppConfiguration.Core;
using AppConfiguration.Core.Base;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AppConfiguration.Sections.Endpoints
{
    [JsonObject(MemberSerialization.OptIn)]
    public class EndpointsSection : ASection
    {
        private const string SectionName = "endpoints";


        [JsonProperty("useSSL")]
        public bool UseSSL { get; set; }

        [JsonProperty("urls")]
        public Dictionary<Url, string> Urls { get; set; }


        public static EndpointsSection Instance { get; } = AppConfigurationManager
            .Configuration
            .GetSection<EndpointsSection>(SectionName);
    }
}
