﻿namespace AppConfiguration
{
    public static class ConfigurationFiles
    {
        #region AppSettings

        public const string AppSettings = "appSettings.json";

        #endregion

        #region Configs

        public const string Logger = "logger.json";
        public const string Endpoints = "endpoints.json";

        #endregion
    }
}