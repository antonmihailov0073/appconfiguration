﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System;
using AppConfiguration.Core.Interfaces;

namespace AppConfiguration.Core
{
    public class AppConfigurationBuilder
    {
        private static class Paths
        {
            public const string Configs = "Configs";
            public const string Shared = "Shared";
            public const string Local = "Local";
        }

        public static readonly string BaseDirectory;
        public static readonly string StandartConfigsPath;


        private string _basePath;


        static AppConfigurationBuilder()
        {
            BaseDirectory = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
            StandartConfigsPath = Path.Combine(BaseDirectory, Paths.Configs);
        }

        public AppConfigurationBuilder()
        {
            AddedFiles = new List<string>();
        }


        internal List<string> AddedFiles { get; set; }


        public AppConfigurationBuilder SetBasePath(string path)
        {
            _basePath = path;
            return this;
        }

        public AppConfigurationBuilder UseStandartSharedPath()
        {
            return SetBasePath(Path.Combine(StandartConfigsPath, Paths.Shared));
        }

        public AppConfigurationBuilder UseStandartLocalPath()
        {
            return SetBasePath(Path.Combine(StandartConfigsPath, Paths.Local));
        }


        public AppConfigurationBuilder IncludeJson(string filePath)
        {
            AddedFiles.Add(Path.Combine(_basePath, filePath));
            return this;
        }

        public AppConfigurationBuilder IncludeJsons(params string[] filePaths)
        {
            Array.ForEach(filePaths, p => IncludeJson(p));
            return this;
        }

        public IAppConfiguration Build()
        {
            return AppConfiguration.Create(LoadConfigFiles());
        }


        private JObject LoadConfigFiles()
        {
            var jsonView = new JObject();
            foreach (var path in AddedFiles)
            {
                MergeJson(jsonView, path, true);
                MergeJson(
                    jsonView,
                    Path.Combine(Path.GetDirectoryName(path), $"{Path.GetFileNameWithoutExtension(path)}.{AppConfiguration.BuildEnvironment}.json"),
                    false);
            }
            return jsonView;
        }


        private static void MergeJson(JObject jsonView, string path, bool required)
        {
            if (required || File.Exists(path))
            {
                jsonView.Merge(JObject.Parse(File.ReadAllText(path)), new JsonMergeSettings
                {
                    MergeArrayHandling = MergeArrayHandling.Replace,
                    MergeNullValueHandling = MergeNullValueHandling.Merge
                });
            }
        }
    }
}
