﻿using AppConfiguration.Common;
using AppConfiguration.Core.Interfaces;

namespace AppConfiguration.Core
{
    public static class AppConfigurationManager
    {
        static AppConfigurationManager()
        {
            InitializeUsingLoader(DependencyProvider.Resolve<IConfigurationLoader>());
        }


        public static IAppConfiguration Configuration { get; private set; }

        public static AppSettingsDictionary AppSettings { get; private set; }

        public static SettingsDictionary ConnectionStrings { get; private set; }
        

        private static void InitializeUsingLoader(IConfigurationLoader loader)
        {
            Configuration = loader.Load();
            AppSettings = Configuration.AppSettings;
            ConnectionStrings = Configuration.ConnectionStrings;
        }
    }
}