﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using AppConfiguration.Core.Attributes;

namespace AppConfiguration.Core
{
    public class AppSettingsDictionary : SettingsDictionary
    {
        public AppSettingsDictionary(Dictionary<string, string> settings) : base(settings) { }


        public T Create<T>() where T : class, new()
        {
            var result = new T();
            var properties = typeof(T).GetProperties()
                .Where(property => property.CanWrite);

            foreach (var property in properties)
            {
                var attr = property.GetCustomAttribute<AppKeyAttribute>();
                if (attr == null || !Settings.ContainsKey(attr.Key)) continue;

                var setting = Settings[attr.Key];
                TrySetValue(result, property, setting);
            }

            return result;
        }


        private void TrySetValue<T>(T obj, PropertyInfo property, string value)
        {
            var propertyType = property.PropertyType;
            if (!typeof(IConvertible).IsAssignableFrom(propertyType)) return;

            var converter = TypeDescriptor.GetConverter(propertyType);
            if (!converter.IsValid(value)) return;

            property.SetValue(obj, Convert.ChangeType(value, propertyType, CultureInfo.InvariantCulture));
        }
    }
}
