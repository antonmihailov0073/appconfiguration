﻿namespace AppConfiguration.Core.Interfaces
{
    public interface IConfigurationLoader
    {
        IAppConfiguration Load();
    }
}
