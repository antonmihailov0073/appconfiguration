﻿using AppConfiguration.Core.Base;

namespace AppConfiguration.Core.Interfaces
{
    public interface IAppConfiguration
    {
        AppSettingsDictionary AppSettings { get; }

        SettingsDictionary ConnectionStrings { get; }

        TSection GetSection<TSection>(string name) where TSection : ASection;

        void SaveToFile(string path, string fileName);
    }
}
