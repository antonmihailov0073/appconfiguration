﻿using AppConfiguration.Core.Interfaces;

namespace AppConfiguration.Core.Base
{
    public abstract class AConfigurationLoader : IConfigurationLoader
    {
        private AppConfigurationBuilder _builder;

        protected abstract void OnLoading(AppConfigurationBuilder builder);


        public IAppConfiguration Load()
        {
            return (_builder ?? (_builder = GetConfiguredBuilder())).Build();
        }


        internal void SetBuilder(AppConfigurationBuilder builder)
        {
            _builder = builder;
        }  

        internal AppConfigurationBuilder GetConfiguredBuilder()
        {
            var builder = new AppConfigurationBuilder();
            OnLoading(builder);
            return builder;
        }
    }
}
