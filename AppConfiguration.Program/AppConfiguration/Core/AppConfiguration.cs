﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using AppConfiguration.Core.Interfaces;
using AppConfiguration.Core.Base;

namespace AppConfiguration.Core
{
    public class AppConfiguration : IAppConfiguration
    {
        private static class Keys
        {
            public const string AppSettings = "appSettings";
            public const string ConnectionStrings = "connectionStrings";
        }


        private static JObject _jsonObject;

        static AppConfiguration()
        {
            BuildEnvironment = GetEnvironment();
        }


        private AppConfiguration(JObject jsonObject)
        {
            _jsonObject = jsonObject;

            AppSettings = LoadSettings<AppSettingsDictionary>(jsonObject, Keys.AppSettings);
            ConnectionStrings = LoadSettings<SettingsDictionary>(jsonObject, Keys.ConnectionStrings);
        }


        public static IAppConfiguration Create(JObject jsonObject)
        {
            return jsonObject != null
                ? new AppConfiguration(jsonObject)
                : throw new ArgumentNullException(nameof(jsonObject));
        }


        public SettingsDictionary ConnectionStrings { get; }

        public AppSettingsDictionary AppSettings { get; }


        public static string BuildEnvironment { get; }


        public TSection GetSection<TSection>(string name)
            where TSection : ASection
        {
            return _jsonObject[name].ToObject<TSection>();
        }
        
        public void SaveToFile(string path, string fileName)
        {
            var json = _jsonObject.ToString(Formatting.Indented);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            File.WriteAllText(Path.Combine(path, fileName), json);
        }


        private static TSettings LoadSettings<TSettings>(JObject jsonObject, string key)
            where TSettings : SettingsDictionary
        {
            return jsonObject.TryGetValue(key, out JToken token)
                ? (TSettings)Activator.CreateInstance(typeof(TSettings), token.ToObject<Dictionary<string, string>>())
                : null;
        }

        private static string GetEnvironment()
        {
            #if DEBUG
                return "debug";
            #elif DEV
                return "dev";
            #elif TEST
                return "test";
            #elif LIVE
                return "live";
            #elif LOCAL
                return "local";
            #elif STAGE
                return "stage";
            #elif RELEASE
                return "release";
            #endif
        }
    }
}
