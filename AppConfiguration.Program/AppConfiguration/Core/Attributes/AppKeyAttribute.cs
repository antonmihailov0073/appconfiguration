﻿using System;

namespace AppConfiguration.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class AppKeyAttribute : Attribute
    {
        public AppKeyAttribute(string key)
        {
            Key = key;
        }


        public string Key { get; }
    }
}
