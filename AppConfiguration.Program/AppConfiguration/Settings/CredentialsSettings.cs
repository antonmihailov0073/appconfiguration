﻿using AppConfiguration.Core;
using AppConfiguration.Core.Attributes;

namespace AppConfiguration.Settings
{
    public class CredentialsSettings
    {
        [AppKey("credentials:user:name")]
        public string UserName { get; set; }

        [AppKey("credentials:password")]
        public string Password { get; set; }

        [AppKey("credentials:secret:key")]
        public string SecretKey { get; set; }


        public static CredentialsSettings Instance { get; } = AppConfigurationManager
            .AppSettings
            .Create<CredentialsSettings>();
    }
}
